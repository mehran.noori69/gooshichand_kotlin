package com.gooshichand.gckot.fragments

/**
* Created by MaHkOoM on 1/8/2019.
*/

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.gooshichand.gckot.R
import com.gooshichand.gckot.adaptors.HBrandAdapter
import com.gooshichand.gckot.adaptors.HMobileItemAdapter
import com.gooshichand.gckot.models.BrandModels
import com.gooshichand.gckot.models.MobileModels
import com.gooshichand.gckot.models.SlideImages
import com.gooshichand.gckot.tools.APICalls
import com.gooshichand.gckot.tools.Preferences
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.fragment_home.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class HomeFragment : Fragment(), View.OnClickListener {

    private lateinit var activity: AppCompatActivity
    private lateinit var catsAdapter: HBrandAdapter
    private lateinit var recomAdapter: HMobileItemAdapter
    private lateinit var urlMaps: HashMap<String, String>
    private var utility: Utility = Utility()
    private lateinit var rootView: ViewGroup
    private lateinit var myTypeface: Typeface
    private lateinit var preferences: Preferences
    private lateinit var deviceID: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity = this.getActivity() as AppCompatActivity
        preferences = Preferences(this.activity)
        deviceID = preferences.getString("deviceID", "false")
        rootView = inflater.inflate(R.layout.fragment_home, container, false) as ViewGroup
        myTypeface = utility.adjustFont(activity)

        setTypeface(myTypeface)

        rootView.search_icon_in_app.setOnClickListener(this)

        urlMaps = HashMap()
        rootView.slider.setDuration(4000)

        val catsGridViewLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        val catsItems : ArrayList<BrandModels> = ArrayList()
        catsAdapter = HBrandAdapter(activity, catsItems)
        rootView.home_cats_items.adapter = catsAdapter
        rootView.home_cats_items.layoutManager = catsGridViewLayoutManager

        val recomGridViewLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        val recomItems : ArrayList<MobileModels> = ArrayList()
        recomAdapter = HMobileItemAdapter(activity, recomItems)
        rootView.home_recom_items.adapter = recomAdapter
        rootView.home_recom_items.layoutManager = recomGridViewLayoutManager

        makeOfferSectionBeReady()

        rootView.grid_load_fragment_list_advertisement.visibility = View.VISIBLE
//        (getActivity() as MainActivity).initProgressBar()
        serviceCall()

        rootView.horizontalSwipeRefreshLayout.setOnRefreshListener {
            rootView.grid_failed_list_advertisement.visibility = View.GONE
            serviceCall()
        }
        return rootView
    }

    private var webserviceCallback: Callback<JsonObject> = object : Callback<JsonObject> {
        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
            if (response.isSuccessful) {
                val result = response.body()

                val mobileRecomString = result.get("mobile_recommanded")
                val mobileBrandString = result.get("mobile_brands")
                val homeSlideString = result.get("home_slide_images")
                val gson = GsonBuilder().setPrettyPrinting().create()

                val slideImages: ArrayList<SlideImages> =
                        gson.fromJson(homeSlideString.asJsonArray, object : TypeToken<ArrayList<SlideImages>>() {}.type)
                // first we need to parse image links
                for (i in 0 until slideImages.size)
                    urlMaps[i.toString()] = slideImages[i].link
                // second we have to push item images to slider images
                for (name in urlMaps.keys) {
                    val defaultSliderView = DefaultSliderView(getActivity())
                    defaultSliderView.image(urlMaps[name]).scaleType = BaseSliderView.ScaleType.Fit
                    rootView.slider.addSlider(defaultSliderView)
                }

                val mobileItems: ArrayList<MobileModels> =
                        gson.fromJson(mobileRecomString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                val hMobileItemAdapter = rootView.home_recom_items.adapter as HMobileItemAdapter
                reloadMobileItemHorAdapter(hMobileItemAdapter, mobileItems)

                val catsItems: ArrayList<BrandModels> =
                        gson.fromJson(mobileBrandString.asJsonArray, object : TypeToken<ArrayList<BrandModels>>() {}.type)
                val hBrandAdapter = rootView.home_cats_items.adapter as HBrandAdapter
                reloadBrandItemAdapter(hBrandAdapter, catsItems)

            }else{
                Log.e("sa", response.toString())
            }
            rootView.grid_load_fragment_list_advertisement.visibility = View.GONE
        }
        override fun onFailure(call: Call<JsonObject>, t: Throwable) {
            if (call.isCanceled){
                // if service call is canceled
            }
            t.printStackTrace()
            rootView.grid_failed_list_advertisement.visibility = View.VISIBLE
            rootView.horizontalSwipeRefreshLayout.setRefreshing(false)
        }
    }

    private fun serviceCall() {
        val retrofit = Retrofit.Builder()
                .baseUrl(activity.getString(R.string.serviceBaseURL))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val apiCalls = retrofit.create(APICalls::class.java)
        apiCalls.getHomePage().enqueue(webserviceCallback)
//        we can cancel this api call with using this cancel function
//        apiCalls.getHomePage().cancel()
    }

    private fun makeOfferSectionBeReady() {
        rootView.home_recom_title.text = utility.offerName
        if (utility.hasOffer) {
            rootView.home_recom_title.visibility = View.VISIBLE
            rootView.home_recom_items.visibility = View.VISIBLE
            rootView.home_recom_ll.visibility = View.VISIBLE
        } else {
            rootView.home_recom_title.visibility = View.GONE
            rootView.home_recom_items.visibility = View.GONE
            rootView.home_recom_ll.visibility = View.GONE
        }
    }

    private fun setTypeface(myTypeface: Typeface) {
        rootView.home_cats_title.typeface = myTypeface
        rootView.home_recom_title.typeface = myTypeface
        rootView.search_text_in_app.typeface = myTypeface
        rootView.grid_failed_list_advertisement.typeface = myTypeface
    }

    private fun reloadBrandItemAdapter(hBrandAdapter : HBrandAdapter, catsItems : ArrayList<BrandModels>) {
        hBrandAdapter.advertItems.clear()
        hBrandAdapter.advertItems.addAll(catsItems)
        hBrandAdapter.notifyDataSetChanged()
        rootView.horizontalSwipeRefreshLayout.setRefreshing(false)
    }

    private fun reloadMobileItemHorAdapter(hMobileItemAdapter : HMobileItemAdapter, catsItems : ArrayList<MobileModels>) {
        hMobileItemAdapter.advertItems.clear()
        hMobileItemAdapter.advertItems.addAll(catsItems)
        hMobileItemAdapter.notifyDataSetChanged()
        rootView.horizontalSwipeRefreshLayout.setRefreshing(false)
    }

    override fun onClick(v: View) {
        when (v.id){
            R.id.search_icon_in_app -> {
                val fragment = MobileItemsFragment()
                val bundle = Bundle()
                bundle.putString("which", "search")
                bundle.putString("query", rootView.search_text_in_app.text.toString())
                fragment.arguments = bundle
                val fragmentManager = getActivity()!!.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
        }
    }

}