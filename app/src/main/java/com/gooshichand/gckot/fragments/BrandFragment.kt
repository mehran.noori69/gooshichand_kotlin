package com.gooshichand.gckot.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.gooshichand.gckot.MainActivity
import com.gooshichand.gckot.R
import com.gooshichand.gckot.adaptors.HMobileItemAdapter
import com.gooshichand.gckot.models.MobileModels
import com.gooshichand.gckot.tools.APICalls
import com.gooshichand.gckot.tools.Preferences
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.fragment_brand.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
* Created by MaHkOoM on 1/9/2019.
*/

class BrandFragment: Fragment(), View.OnClickListener {

    private lateinit var activity: AppCompatActivity
    private lateinit var catsAdapter: HMobileItemAdapter
    private lateinit var newAdapter: HMobileItemAdapter
    private lateinit var cpuAdapter: HMobileItemAdapter
    private lateinit var camerabAdapter: HMobileItemAdapter
    private lateinit var camerafAdapter: HMobileItemAdapter
    private lateinit var ramAdapter: HMobileItemAdapter
    private lateinit var screenAdapter: HMobileItemAdapter
    private lateinit var weightAdapter: HMobileItemAdapter
    private lateinit var brand: String
    private lateinit var appTitle: String
    private var utility: Utility = Utility()
    private lateinit var rootView: ViewGroup

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity = this.getActivity() as AppCompatActivity
        rootView = inflater.inflate(R.layout.fragment_brand, container, false) as ViewGroup

        val bundle = this.arguments
        if (bundle != null) {
            val temp: String? = bundle.getString("brand")
            if (temp != null)
                brand = temp
        }

        appTitle = utility.changeAppTitle(brand, activity as MainActivity)

        adjustFont(utility.adjustFont(activity))

        rootView.grid_load_fragment_list_advertisement.visibility = View.VISIBLE

        prepareGridViews()

        serviceCall(brand)

        rootView.horizontalSwipeRefreshLayout.setOnRefreshListener {
            rootView.grid_failed_list_advertisement.visibility = View.GONE
            serviceCall(brand)
        }

        return rootView
    }

    private fun prepareGridViews() {
        catsAdapter = HMobileItemAdapter(activity, ArrayList())
        newAdapter = HMobileItemAdapter(activity, ArrayList())
        cpuAdapter = HMobileItemAdapter(activity, ArrayList())
        camerabAdapter = HMobileItemAdapter(activity, ArrayList())
        camerafAdapter = HMobileItemAdapter(activity, ArrayList())
        ramAdapter = HMobileItemAdapter(activity, ArrayList())
        screenAdapter = HMobileItemAdapter(activity, ArrayList())
        weightAdapter = HMobileItemAdapter(activity, ArrayList())

        rootView.brand_cats_items.adapter = catsAdapter
        rootView.brand_new_items.adapter = newAdapter
        rootView.brand_cpu_items.adapter = cpuAdapter
        rootView.brand_camerab_items.adapter = camerabAdapter
        rootView.brand_cameraf_items.adapter = camerafAdapter
        rootView.brand_ram_items.adapter = ramAdapter
        rootView.brand_screen_items.adapter = screenAdapter
        rootView.brand_weight_items.adapter = weightAdapter

        rootView.brand_cats_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_new_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_cpu_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_camerab_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_cameraf_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_ram_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_screen_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rootView.brand_weight_items.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    }

    private var webserviceCallback: Callback<JsonObject> = object : Callback<JsonObject> {
        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
            if (response.isSuccessful) {
                val result = response.body()
                val frontCameraString = result.get("front_camera")
                val cpuString = result.get("cpu")
                val ramString = result.get("ram")
                val inchString = result.get("inch")
                val weightString = result.get("weight")
                val newestString = result.get("newest")
                val backCameraString = result.get("back_camera")
                val gson = GsonBuilder().setPrettyPrinting().create()

                // loading fc mobile items
                val fcItems: ArrayList<MobileModels> =
                        gson.fromJson(frontCameraString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                var hMobileItemAdapter = rootView.brand_cameraf_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, fcItems)

                // loading cpu mobile items
                val cpuItems: ArrayList<MobileModels> =
                        gson.fromJson(cpuString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                hMobileItemAdapter = rootView.brand_cpu_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, cpuItems)

                // loading ram mobile items
                val ramItems: ArrayList<MobileModels> =
                        gson.fromJson(ramString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                hMobileItemAdapter = rootView.brand_ram_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, ramItems)

                // loading inch mobile items
                val inchItems: ArrayList<MobileModels> =
                        gson.fromJson(inchString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                hMobileItemAdapter = rootView.brand_screen_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, inchItems)

                // loading weight mobile items
                val weightItems: ArrayList<MobileModels> =
                        gson.fromJson(weightString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                hMobileItemAdapter = rootView.brand_weight_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, weightItems)

                // loading new mobile items
                val newItems: ArrayList<MobileModels> =
                        gson.fromJson(newestString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                hMobileItemAdapter = rootView.brand_new_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, newItems)

                // loading back camera mobile items
                val bcItems: ArrayList<MobileModels> =
                        gson.fromJson(backCameraString.asJsonArray, object : TypeToken<ArrayList<MobileModels>>() {}.type)
                hMobileItemAdapter = rootView.brand_camerab_items.adapter as HMobileItemAdapter
                reloadMobileHAdapter(hMobileItemAdapter, bcItems)
            }
            rootView.grid_load_fragment_list_advertisement.visibility = View.GONE
        }

        override fun onFailure(call: Call<JsonObject>, t: Throwable) {
            t.printStackTrace()
            rootView.grid_failed_list_advertisement.visibility = View.VISIBLE
            rootView.horizontalSwipeRefreshLayout.setRefreshing(false)
        }
    }

    private fun serviceCall(brand: String) {
        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()
        val retrofit = Retrofit.Builder()
                .baseUrl(activity.getString(R.string.serviceBaseURL))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        val apiCalls = retrofit.create(APICalls::class.java)
        apiCalls.getBrandPage(brand).enqueue(webserviceCallback)
    }

    private fun adjustFont(myTypeface: Typeface) {
        rootView.brand_cats_title.typeface = myTypeface
        rootView.brand_new_title.typeface = myTypeface
        rootView.brand_cpu_title.typeface = myTypeface
        rootView.brand_camerab_title.typeface = myTypeface
        rootView.brand_cameraf_title.typeface = myTypeface
        rootView.brand_ram_title.typeface = myTypeface
        rootView.brand_screen_title.typeface = myTypeface
        rootView.brand_weight_title.typeface = myTypeface
        rootView.brand_cats_more.typeface = myTypeface
        rootView.brand_new_more.typeface = myTypeface
        rootView.brand_cpu_more.typeface = myTypeface
        rootView.brand_camerab_more.typeface = myTypeface
        rootView.brand_cameraf_more.typeface = myTypeface
        rootView.brand_ram_more.typeface = myTypeface
        rootView.brand_screen_more.typeface = myTypeface
        rootView.brand_weight_more.typeface = myTypeface
        rootView.grid_failed_list_advertisement.typeface = myTypeface
        rootView.brand_cats_more.setOnClickListener(this)
        rootView.brand_new_more.setOnClickListener(this)
        rootView.brand_cpu_more.setOnClickListener(this)
        rootView.brand_camerab_more.setOnClickListener(this)
        rootView.brand_cameraf_more.setOnClickListener(this)
        rootView.brand_ram_more.setOnClickListener(this)
        rootView.brand_screen_more.setOnClickListener(this)
        rootView.brand_weight_more.setOnClickListener(this)
    }

    private fun reloadMobileHAdapter(hMobileItemAdapter: HMobileItemAdapter, catsItems: ArrayList<MobileModels>) {
        hMobileItemAdapter.advertItems.clear()
        hMobileItemAdapter.advertItems.addAll(catsItems)
        hMobileItemAdapter.notifyDataSetChanged()
        rootView.horizontalSwipeRefreshLayout.setRefreshing(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (activity as MainActivity? != null) {
            (activity as MainActivity).getAppTitle().text = this.appTitle
        }
    }

    override fun onClick(v: View) {
        when {
            v.id == R.id.brand_cats_more -> {
                val bundle = Bundle()
                bundle.putString("which", "newest")
                bundle.putString("title", "جدیدترین")
                bundle.putString("brand", brand)
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_new_more -> {
                val bundle = Bundle()
                bundle.putString("which", "newest")
                bundle.putString("title", "جدیدترین")
                bundle.putString("brand", brand)
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_camerab_more -> {
                val bundle = Bundle()
                bundle.putString("which", "back_camera")
                bundle.putString("title", "قوی ترین دوربین عقب")
                bundle.putString("brand", brand)
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_cameraf_more -> {
                val bundle = Bundle()
                bundle.putString("which", "front_camera")
                bundle.putString("title", "قوی ترین دوربین جلو")
                bundle.putString("brand", brand)
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_cpu_more -> {
                val bundle = Bundle()
                bundle.putString("which", "cpu")
                bundle.putString("title", "قوی ترین پردازنده")
                bundle.putString("brand", brand)
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_ram_more -> {
                val bundle = Bundle()
                bundle.putString("which", "ram")
                bundle.putString("title", "بیشترین رم")
                bundle.putString("brand", brand)
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_screen_more -> {
                val bundle = Bundle()
                bundle.putString("which", "inch")
                bundle.putString("brand", brand)
                bundle.putString("title", "بزرگترین صفحه نمایش")
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
            v.id == R.id.brand_weight_more -> {
                val bundle = Bundle()
                bundle.putString("which", "weight")
                bundle.putString("brand", brand)
                bundle.putString("title", "سبک ترین")
                val fragment = MobileItemsFragment()
                fragment.arguments = bundle
                val fragmentManager = activity.supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit()
            }
        }
    }
}