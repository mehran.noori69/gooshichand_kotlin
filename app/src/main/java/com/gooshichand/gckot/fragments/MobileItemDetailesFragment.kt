package com.gooshichand.gckot.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.gooshichand.gckot.MainActivity
import com.gooshichand.gckot.R
import com.gooshichand.gckot.models.MobileModels
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.fragment_mobile_detailes.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
* Created by MaHkOoM on 1/9/2019.
*/

class MobileItemDetailesFragment : Fragment() , BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private lateinit var activity: AppCompatActivity
    private var utility: Utility = Utility()
    private lateinit var appTitle: String
    private lateinit var mobileModel: MobileModels
    private lateinit var rootView: ViewGroup

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity = this.getActivity() as AppCompatActivity
        rootView = inflater.inflate(R.layout.fragment_mobile_detailes, container, false) as ViewGroup
        val myTypeface = utility.adjustFont(activity)

        val bundle = this.arguments
        if (bundle != null) {
            mobileModel = bundle.getSerializable("item") as MobileModels
            appTitle = (activity as MainActivity).getAppTitle().text.toString()
            (activity as MainActivity).getAppTitle().text = mobileModel.title
        }

        setTypeface(myTypeface)
        initialValues()

        rootView.mobile_detail_slider.setDuration(4000)
        rootView.mobile_detail_slider.addOnPageChangeListener(this)
        val galleries = mobileModel.gallery.split(",")
        for (gallery in galleries) {
            val defaultSliderView = DefaultSliderView(activity)
            defaultSliderView.image(gallery)
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(this)
            rootView.mobile_detail_slider.addSlider(defaultSliderView)
        }

        return rootView
    }

    private fun initialValues() {
        rootView.mobile_detail_title.text = String.format("%s : %s", getString(R.string.mobile_title), mobileModel.title)
        rootView.mobile_detail_os.text = String.format("%s : %s", getString(R.string.mobile_os), utility.toPersianNumber(mobileModel.os))
        val formatter = SimpleDateFormat("yyyy-mm-dd HH:mm:ss", Locale.ENGLISH)
        try {
            val date = formatter.parse(mobileModel.releaseDate)
            rootView.mobile_detail_release.text = String.format("%s : %s - %s", getString(R.string.mobile_release_date),
                    utility.toPersianNumber(date.toGMTString().split(" ")[1]), utility.toPersianNumber(date.toGMTString().split(" ")[2]))
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        rootView.mobile_detail_bc.text = String.format("%s : %s %s", getString(R.string.mobile_back_camera), utility.toPersianNumber(mobileModel.cameraBack), getString(R.string.megapixel))
        rootView.mobile_detail_fc.text = String.format("%s : %s %s", getString(R.string.mobile_back_camera), utility.toPersianNumber(mobileModel.cameraFront), getString(R.string.megapixel))

        rootView.mobile_detail_dim.text = String.format("%s : %s %s", getString(R.string.mobile_dim), utility.toPersianNumber(mobileModel.dims), "")
        rootView.mobile_detail_weight.text = String.format("%s : %s %s", getString(R.string.mobile_weight), utility.toPersianNumber(mobileModel.weight), getString(R.string.gram))
        rootView.mobile_detail_inch.text = String.format("%s : %s %s", getString(R.string.mobile_inch), utility.toPersianNumber(mobileModel.inch), getString(R.string.inch))
        rootView.mobile_detail_pd.text = String.format("%s : %s %s", getString(R.string.mobile_pd), utility.toPersianNumber(mobileModel.pd), getString(R.string.ppi))
        rootView.mobile_detail_reso.text = String.format("%s : %s %s", getString(R.string.mobile_reso), utility.toPersianNumber(mobileModel.reso), getString(R.string.pixel))
        rootView.mobile_detail_stbratio.text = String.format("%s : %s %s", getString(R.string.mobile_stbratio), utility.toPersianNumber(mobileModel.stbRatio), getString(R.string.ratio))
        rootView.mobile_detail_memory.text = String.format("%s : %s %s", getString(R.string.mobile_memory), utility.toPersianNumber(mobileModel.memory), "")
        rootView.mobile_detail_gpu.text = String.format("%s : %s %s", getString(R.string.mobile_gpu), utility.toPersianNumber(mobileModel.gpu), "")
        rootView.mobile_detail_data.text = String.format("%s : %s %s", getString(R.string.mobile_data), utility.toPersianNumber(mobileModel.data), "")
        rootView.mobile_detail_nano.text = String.format("%s : %s %s", getString(R.string.mobile_nano), mobileModel.nano, "")
        rootView.mobile_detail_bluetooth.text = String.format("%s : %s %s", getString(R.string.mobile_bluetooth), getString(R.string.version), utility.toPersianNumber(mobileModel.bluetooth))
        rootView.mobile_detail_battery_cap.text = String.format("%s : %s %s", getString(R.string.mobile_battery_cap), utility.toPersianNumber(mobileModel.batteryCapacity), getString(R.string.mAh))
        rootView.mobile_detail_cpu.text = String.format("%s : %s %s", getString(R.string.mobile_cpu), utility.toPersianNumber(mobileModel.cpuProcessor), "")
        rootView.mobile_detail_cpu_core.text = String.format("%s : %s %s", getString(R.string.mobile_cpu_core), utility.toPersianNumber(mobileModel.cpuCore), "")
        rootView.mobile_detail_cpu_freq.text = String.format("%s : %s %s", getString(R.string.mobile_cpu_freq), utility.toPersianNumber(mobileModel.cpuFreq), getString(R.string.Hertz))
    }

    private fun setTypeface(myTypeface: Typeface) {
        rootView.mobile_detail_title.typeface = myTypeface
        rootView.mobile_detail_os.typeface = myTypeface
        rootView.mobile_detail_release.typeface = myTypeface
        rootView.mobile_detail_bc.typeface = myTypeface
        rootView.mobile_detail_fc.typeface = myTypeface
        rootView.mobile_detail_dim.typeface = myTypeface
        rootView.mobile_detail_weight.typeface = myTypeface
        rootView.mobile_detail_inch.typeface = myTypeface
        rootView.mobile_detail_pd.typeface = myTypeface
        rootView.mobile_detail_reso.typeface = myTypeface
        rootView.mobile_detail_stbratio.typeface = myTypeface
        rootView.mobile_detail_memory.typeface = myTypeface
        rootView.mobile_detail_gpu.typeface = myTypeface
        rootView.mobile_detail_data.typeface = myTypeface
        rootView.mobile_detail_nano.typeface = myTypeface
        rootView.mobile_detail_bluetooth.typeface = myTypeface
        rootView.mobile_detail_battery_cap.typeface = myTypeface
        rootView.mobile_detail_cpu.typeface = myTypeface
        rootView.mobile_detail_cpu_core.typeface = myTypeface
        rootView.mobile_detail_cpu_freq.typeface = myTypeface
    }

    override fun onDestroy() {
        super.onDestroy()
        if (getActivity() != null) {
            (getActivity() as MainActivity).getAppTitle().text = this.appTitle
        }
    }

    override fun onSliderClick(slider: BaseSliderView) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }

}