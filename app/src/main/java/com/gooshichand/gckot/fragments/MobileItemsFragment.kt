package com.gooshichand.gckot.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import com.gooshichand.gckot.MainActivity
import com.gooshichand.gckot.R
import com.gooshichand.gckot.adaptors.VMobileItemAdapter
import com.gooshichand.gckot.models.MobileModels
import com.gooshichand.gckot.tools.APICalls
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.fragment_mobile_items.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
* Created by MaHkOoM on 1/9/2019.
*/

class MobileItemsFragment : Fragment() {

    private lateinit var activity: AppCompatActivity
    private lateinit var mobileListViewAdapter: VMobileItemAdapter
    private var loading: Boolean = false
    private var lastFirstItemPosition: Int = 0
    private var currentFirstItemPosition: Int = 0
    private var index: Int = 0
    private lateinit var appTitle: String
    private lateinit var section: String
    private lateinit var query: String
    private lateinit var brand: String
    private lateinit var header: String
    private var utility: Utility = Utility()
    private var paginationSize : Int = 20
    private var isPulled : Boolean = false
    private lateinit var rootView: ViewGroup

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity = this.getActivity() as AppCompatActivity

        rootView = inflater.inflate(R.layout.fragment_mobile_items, container, false) as ViewGroup
        rootView.mobile_item_failed_list.typeface = utility.adjustFont(activity)

        val bundle = arguments
        if (bundle != null) {
            var temp = bundle.getString("which")
            if (temp != null) {
                section = temp
                if (section.equals("search", true)) {
                    temp = bundle.getString("query")
                    if (temp != null) {
                        query = temp
                        header = getString(R.string.search_for) + " " + query
                    }
                } else {
                    temp = bundle.getString("brand")
                    val temp1 = bundle.getString("title")
                    if ( temp != null && temp1 != null){
                        brand = temp
                        header = temp1
                    }
                }
            }
        }
        appTitle = (getActivity() as MainActivity).getAppTitle().text.toString()
        (getActivity() as MainActivity).getAppTitle().text = header

        val layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false)
        val items: java.util.ArrayList<MobileModels> = ArrayList()
        rootView.mobile_item_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView : RecyclerView, dx : Int, dy : Int) {
                if (dy > 0){
                    val visibleItemCount = recyclerView.childCount
                    val totalItemCount = (recyclerView.layoutManager as LinearLayoutManager).itemCount
                    val firstVisiblesItem = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                    currentFirstItemPosition = firstVisiblesItem
                    if (lastFirstItemPosition <= firstVisiblesItem && !loading && totalItemCount != 0
                            && firstVisiblesItem + visibleItemCount >= totalItemCount) {
                        loading = true
                        rootView.mobile_item_load_list.visibility = View.VISIBLE
                        isPulled = false
                        serviceCall()
                    }
                }
            }
        })
        mobileListViewAdapter = VMobileItemAdapter(activity, items)
        rootView.mobile_item_list.adapter = mobileListViewAdapter
        rootView.mobile_item_list.layoutManager = layoutManager

        index = 0
        loading = false
        lastFirstItemPosition = 0

        rootView.mobile_item_load_list.visibility = View.VISIBLE
        isPulled = false
        serviceCall()

        rootView.mobile_item_swipeRefreshLayout.setOnRefreshListener {
            lastFirstItemPosition = 0
            currentFirstItemPosition = 0
            index = 0
            rootView.mobile_item_failed_list.visibility = View.GONE
            isPulled = true
            serviceCall()
        }

        refreshGridView()
        return rootView
    }

    private var webserviceCallback: Callback<JsonArray> = object : Callback<JsonArray> {
        override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
            if (response.isSuccessful) {
                val result = response.body()
                val gson = GsonBuilder().setPrettyPrinting().create()
                val mobileItems: java.util.ArrayList<MobileModels> =
                        gson.fromJson(result.asJsonArray, object : TypeToken<java.util.ArrayList<MobileModels>>() {}.type)

                val vMobileItemAdapter = rootView.mobile_item_list.adapter as VMobileItemAdapter
                reloadMobileHAdapter(vMobileItemAdapter, mobileItems)
                if (mobileItems.size == 0 && vMobileItemAdapter.advertItems.size == 0)
                    rootView.mobile_item_failed_list.visibility = View.VISIBLE

            }else{
                Log.e("sa", response.toString())
            }
            rootView.mobile_item_load_list.visibility = View.GONE
            loading = false
            lastFirstItemPosition = currentFirstItemPosition
        }
        override fun onFailure(call: Call<JsonArray>, t: Throwable) {
            t.printStackTrace()
            rootView.mobile_item_failed_list.visibility = View.VISIBLE
            rootView.mobile_item_swipeRefreshLayout.setRefreshing(false)
        }
    }

    private fun serviceCall() {
        val retrofit = Retrofit.Builder()
                .baseUrl(activity.getString(R.string.serviceBaseURL))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val apiCalls = retrofit.create(APICalls::class.java)
        this.paginationSize
        if (section.equals("search", true))
            apiCalls.searchItemByTitle(index.toString(), this.query).enqueue(webserviceCallback)
        else
            apiCalls.estItemByTitle(index.toString(), section, brand).enqueue(webserviceCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as MainActivity).getAppTitle().text = this.appTitle
    }

    private fun reloadMobileHAdapter(vMobileItemAdapter: VMobileItemAdapter, catsItems: ArrayList<MobileModels>) {
        if (isPulled)
            vMobileItemAdapter.advertItems.clear()
        vMobileItemAdapter.advertItems.addAll(catsItems)
        vMobileItemAdapter.notifyDataSetChanged()
        rootView.mobile_item_swipeRefreshLayout.setRefreshing(false)
    }

    private fun refreshGridView() {
        val display = getActivity()!!.windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val density = resources.displayMetrics.density
        val dpWidth = outMetrics.widthPixels / density
        val columns = Math.round(dpWidth / 300)
        val mLayoutManager = GridLayoutManager(getActivity(), columns)
        rootView.mobile_item_list.layoutManager = mLayoutManager
    }

}
