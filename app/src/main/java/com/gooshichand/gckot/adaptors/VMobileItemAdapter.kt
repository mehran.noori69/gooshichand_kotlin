package com.gooshichand.gckot.adaptors

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.gooshichand.gckot.R
import com.gooshichand.gckot.adaptors.VMobileItemAdapter.SimpleViewHolder
import com.gooshichand.gckot.fragments.MobileItemDetailesFragment
import com.gooshichand.gckot.models.MobileModels
import com.gooshichand.gckot.tools.Preferences
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.view_v_mobile_elements.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
* Created by MaHkOoM on 1/9/2019.
*/

class VMobileItemAdapter(private var activity: AppCompatActivity, items: ArrayList<MobileModels>) : RecyclerView.Adapter<SimpleViewHolder>(){

    private var myTypeface: Typeface
    var advertItems: ArrayList<MobileModels> = ArrayList()
    private var preferences: Preferences
    private var utility: Utility = Utility()

    init {
        advertItems = ArrayList()
        advertItems.addAll(items)
        myTypeface = utility.adjustFont(activity)
        preferences = Preferences(activity)
    }

    inner class SimpleViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(activity).inflate(R.layout.view_v_mobile_elements, parent, false)
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.itemView.v_mobile_name.text = advertItems[position].title
        holder.itemView.v_mobile_brand.text = String.format("%s : %s", activity.getString(R.string.mobile_brand), utility.toPersianNumber(advertItems[position].path))
        holder.itemView.v_mobile_os.text = String.format("%s : %s", activity.getString(R.string.mobile_os), utility.toPersianNumber(advertItems[position].os))
        val formatter = SimpleDateFormat("yyyy-mm-dd HH:mm:ss", Locale.ENGLISH)
        val date = formatter.parse(advertItems[position].releaseDate)
        holder.itemView.v_mobile_year.text = String.format("%s : %s", activity.getString(R.string.mobile_release_date),
                utility.toPersianNumber(date.toGMTString().split(" ")[2]))
        holder.itemView.v_mobile_name.typeface = myTypeface
        holder.itemView.v_mobile_brand.typeface = myTypeface
        holder.itemView.v_mobile_os.typeface = myTypeface
        holder.itemView.v_mobile_year.typeface = myTypeface

        if (!advertItems[position].image.equals("", true)) {
            Glide
                    .with(activity)
                    .load(advertItems[position].image)
                    .apply(RequestOptions().placeholder(R.drawable.no_image_available).error((R.drawable.no_image_available))
                            .centerCrop().diskCacheStrategy(DiskCacheStrategy.DATA))
                    .into(holder.itemView.v_mobile_image)
        }

        holder.itemView.v_mobile_image.setOnClickListener {
            val fragment = MobileItemDetailesFragment()
            val bundle = Bundle()
            bundle.putSerializable("item", advertItems[position])
            fragment.arguments = bundle
            val fragmentManager = activity.supportFragmentManager
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit()
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return this.advertItems.size
    }
}