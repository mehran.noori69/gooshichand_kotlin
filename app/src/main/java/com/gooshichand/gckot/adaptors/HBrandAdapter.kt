package com.gooshichand.gckot.adaptors

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.gooshichand.gckot.R
import com.gooshichand.gckot.adaptors.HBrandAdapter.SimpleViewHolder
import com.gooshichand.gckot.fragments.BrandFragment
import com.gooshichand.gckot.models.BrandModels
import com.gooshichand.gckot.tools.Preferences
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.view_h_brand_elements.view.*
import java.util.*


/**
* Created by MaHkOoM on 1/8/2019.
*/

class HBrandAdapter(private var activity: AppCompatActivity, items: ArrayList<BrandModels>) : RecyclerView.Adapter<SimpleViewHolder>(){

    private var myTypeface: Typeface
    var advertItems: ArrayList<BrandModels> = ArrayList()
    private var preferences: Preferences
    private var utility: Utility = Utility()

    init {
        advertItems.addAll(items)
        myTypeface = utility.adjustFont(activity)
        preferences = Preferences(activity)
    }

    inner class SimpleViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(this.activity).inflate(R.layout.view_h_brand_elements, parent, false)
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.itemView.h_brand_name.text = advertItems[position].name
        holder.itemView.h_brand_name.typeface = myTypeface

        if (!advertItems[position].image.equals("", true)) {
            Glide
                    .with(activity)
                    .load(advertItems[position].image)
                    .apply(RequestOptions().placeholder(R.drawable.no_image_available).error((R.drawable.no_image_available))
                            .centerCrop().diskCacheStrategy(DiskCacheStrategy.DATA))
                    .into(holder.itemView.h_brand_image)
        }

        holder.itemView.h_brand_image.setOnClickListener {
            val fragment = BrandFragment()
            val bundle = Bundle()
            bundle.putString("brand", advertItems[position].name)
            fragment.arguments = bundle
            val fragmentManager = activity.supportFragmentManager
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit()
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return this.advertItems.size
    }
}