package com.gooshichand.gckot.drawer

/**
* Created by MaHkOoM on 1/9/2019.
*/

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.gooshichand.gckot.R
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.fragment_about_us.view.*


class AboutUsDialogFragment : DialogFragment() {

    private var utility: Utility = Utility()
    private lateinit var activity: AppCompatActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity = this.getActivity() as AppCompatActivity
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        val rootView = inflater.inflate(R.layout.fragment_about_us, container, false) as ViewGroup
        val myTypeface = utility.adjustFont(activity)
        rootView.fragmentAboutUsBody.typeface = myTypeface
        return rootView
    }

    override fun onStart() {
        super.onStart()
        if (dialog == null)
            return
        val wm = getActivity()!!.baseContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = 9 * (size.x / 10)
        val height = 2 * (size.y / 3)
        dialog.window!!.setLayout(width, height)
        dialog.setCanceledOnTouchOutside(true)
    }
}
