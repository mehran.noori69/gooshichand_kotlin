package com.gooshichand.gckot.drawer

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.google.gson.JsonObject
import com.gooshichand.gckot.MainActivity
import com.gooshichand.gckot.R
import com.gooshichand.gckot.tools.APICalls
import com.gooshichand.gckot.tools.Preferences
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.fragment_contact_us.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
* Created by MaHkOoM on 1/9/2019.
*/

class ContactUsFragment : Fragment() ,View.OnClickListener {

    private lateinit var activity: AppCompatActivity
    private var utility: Utility = Utility()
    private lateinit var appTitle: String
    private lateinit var preferences: Preferences
    private lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        initializingField()
        rootView = inflater.inflate(R.layout.fragment_contact_us, container, false)

        setAppTitle()
        adjustFonts()

        (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        rootView.sendCommentTitleText.requestFocus()
        rootView.sendCommentButton.setOnClickListener(this)

        return rootView
    }

    private fun initializingField() {
        activity = this.getActivity() as AppCompatActivity
        preferences = Preferences(activity)
    }

    private fun setAppTitle() {
        (activity as MainActivity).getToggle().visibility = View.GONE
        appTitle = (activity as MainActivity).getAppTitle().text.toString()
        (activity as MainActivity).getAppTitle().setText(R.string.contact_us)
    }

    private var webserviceCallback: Callback<JsonObject> = object : Callback<JsonObject> {
        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
            if (response.isSuccessful) {
                val result = response.body()
                utility.makeSnackBar(activity, result.get("result").toString())
                getActivity()!!.supportFragmentManager.popBackStackImmediate()
                rootView.load_fragment_contact_us.visibility = View.GONE

            }else{
                Log.e("sa", response.toString())
            }
            rootView.load_fragment_contact_us.visibility = View.GONE
        }
        override fun onFailure(call: Call<JsonObject>, t: Throwable) {
            t.printStackTrace()
        }
    }

    private fun serviceCall() {
        val retrofit = Retrofit.Builder()
                .baseUrl(activity.getString(R.string.serviceBaseURL))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val apiCalls = retrofit.create(APICalls::class.java)
        apiCalls.contactUs(rootView.sendCommentTitleText.text.toString(),
                rootView.sendCommentEmailText.text.toString(), "Mobile Contact Us", rootView.sendCommentContextText.text.toString(),
                preferences.getString("deviceID", "false") ).enqueue(webserviceCallback)
    }

    private fun checkIfEmpty(): Boolean {
        if (rootView.sendCommentEmailText.text.toString().isEmpty()) {
            utility.makeSnackBar(activity, getActivity()!!.getString(R.string.noEmail))
            return false
        }
        if (rootView.sendCommentTitleText.text.toString().isEmpty()) {
            utility.makeSnackBar(activity, getActivity()!!.getString(R.string.noTitle))
            return false
        }
        if (rootView.sendCommentContextText.text.toString().isEmpty()) {
            utility.makeSnackBar(activity, getActivity()!!.getString(R.string.noContext))
            return false
        }
        return true
    }

    private fun adjustFonts() {
        val myTypeface = utility.adjustFont(activity)
        rootView.sendCommentEmailText.typeface = myTypeface
        rootView.sendCommentTitleText.typeface = myTypeface
        rootView.sendCommentContextText.typeface = myTypeface
    }

    override fun onClick(v: View) {
        if (v.id == R.id.sendCommentButton) {
            if (checkIfEmpty()) {
                rootView.load_fragment_contact_us.visibility = View.VISIBLE
                serviceCall()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        val view = getActivity()!!.currentFocus
        if (view != null) {
            val imm = getActivity()!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        (getActivity() as MainActivity).getToggle().visibility = View.VISIBLE
        (getActivity() as MainActivity).getAppTitle().text = this.appTitle

    }
}
