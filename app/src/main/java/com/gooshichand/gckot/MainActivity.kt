package com.gooshichand.gckot

import android.content.Intent
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.gooshichand.gckot.drawer.AboutUsDialogFragment
import com.gooshichand.gckot.drawer.ContactUsFragment
import com.gooshichand.gckot.fragments.HomeFragment
import com.gooshichand.gckot.tools.Preferences
import com.gooshichand.gckot.tools.Utility
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), View.OnClickListener, FragmentManager.OnBackStackChangedListener {

    private var isReadyToExit: Boolean = false
    private lateinit var appTitle: TextView
    private lateinit var toggle: ImageView

    private var utility: Utility = Utility()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myTypeface = utility.adjustFont(this)

        setOnClicklistener()
        setTypeface(myTypeface)
        registerIDForDeviceInPreferences()
        changeActionBar(myTypeface)
        loadFragment()
    }

    private fun setTypeface(myTypeface: Typeface) {
        nav_contact_us_txt.typeface = myTypeface
        nav_share_txt.typeface = myTypeface
        nav_about_us_txt.typeface = myTypeface
    }

    private fun setOnClicklistener() {
        nav_contact_us.setOnClickListener(this)
        nav_share.setOnClickListener(this)
        nav_about_us.setOnClickListener(this)
    }

    fun getAppTitle() : TextView {
        return appTitle
    }

    fun getToggle() : ImageView {
        return toggle
    }

    private fun loadFragment() {
        val fragment = HomeFragment()
        val fragmentManager = supportFragmentManager
        fragmentManager.addOnBackStackChangedListener(this)
        while (fragmentManager.backStackEntryCount > 0)
            fragmentManager.popBackStackImmediate()
        fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit()
    }

    private fun changeActionBar(myTypeface: Typeface) {
        appTitle = slidingMenuTitle
        appTitle.typeface = myTypeface
        toggle = slideMenuButton
        toggle.setOnClickListener(this)
        backButton.setOnClickListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStackImmediate()
            return
        }
        if (!isReadyToExit) {
            utility.makeSnackBar(this, getString(R.string.clickBackTwice))
            isReadyToExit = !isReadyToExit
            return
        }
        finish()
    }

    override fun onClick(v: View) {
        when (v.id){
            R.id.slideMenuButton -> {
                if (drawer_layout.isDrawerVisible(GravityCompat.START))
                    drawer_layout.closeDrawer(drawer_layout)
                else
                    drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.nav_contact_us -> {
                val fragment = ContactUsFragment()
                val fragmentManager = supportFragmentManager
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment, "contactus").addToBackStack("fragment").commit()
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.nav_about_us -> {
                val dialogFragment = AboutUsDialogFragment()
                val fragmentManager = supportFragmentManager
                dialogFragment.show(fragmentManager, "Dialog Fragment")
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.nav_share -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_drawer_post_body))
                startActivity(Intent.createChooser(shareIntent, getString(R.string.sharing_drawer_post_title)))
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.backButton -> {
                supportFragmentManager.popBackStackImmediate()
            }
        }
    }

    override fun onBackStackChanged() {
        if (supportFragmentManager.backStackEntryCount >= 2) {
            backButton.visibility = View.VISIBLE
        } else if (supportFragmentManager.backStackEntryCount < 2) {
            backButton.visibility = View.GONE
        }
    }

    private fun registerIDForDeviceInPreferences() {
        val preferences = Preferences(this)
        val deviceID = preferences.getString("deviceID", "false")
        if (deviceID.equals("false", ignoreCase = true)) {
            val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US)
            val currentDate = Date()
            dateFormat.format(currentDate)
            preferences.saveString("deviceID", currentDate.toString())
        }
    }

    private lateinit var progressBar: ProgressBar

    fun initProgressBar() {
        progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleSmall)
        progressBar.isIndeterminate = true
        val params = RelativeLayout.LayoutParams(100 ,Resources.getSystem().displayMetrics.heightPixels)
        params.addRule(RelativeLayout.ALIGN_BOTTOM)
//        params.addRule(RelativeLayout.CENTER_HORIZONTAL)
        this.addContentView(progressBar, params)
        showProgressBar()
    }

    fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

}
