package com.gooshichand.gckot.models

/**
* Created by MaHkOoM on 1/8/2019.
*/

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MobileModels : Serializable{
    lateinit var title: String
    @SerializedName("img")
    lateinit var image: String
    @SerializedName("battery_capacity")
    lateinit var batteryCapacity: String
    @SerializedName("battery_talk_time")
    lateinit var batteryTalkTime: String
    @SerializedName("battery_type")
    lateinit var batteryType: String
    lateinit var bluetooth: String
    @SerializedName("camera_back")
    lateinit var cameraBack: String
    @SerializedName("camera_front")
    lateinit var cameraFront: String
    @SerializedName("cpu_chip")
    lateinit var cpuChip: String
    @SerializedName("cpu_core")
    lateinit var cpuCore: String
    @SerializedName("cpu_freq")
    lateinit var cpuFreq: String
    @SerializedName("cpu_processor")
    lateinit var cpuProcessor: String
    lateinit var data: String
    lateinit var dims: String
    lateinit var gallery: String
    lateinit var gpu: String
    lateinit var hasprice: String
    lateinit var inch: String
    lateinit var link: String
    lateinit var memory: String
    lateinit var minprice: String
    lateinit var nano: String
    lateinit var os: String
    lateinit var path: String
    lateinit var pd: String
    @SerializedName("price_label")
    lateinit var priceLabel: List<String>
    @SerializedName("release_date")
    lateinit var releaseDate: String
    lateinit var reso: String
    @SerializedName("stbratio")
    lateinit var stbRatio: String
    lateinit var storage: String
    lateinit var stores: List<MobileStore>
    @SerializedName("weight")
    lateinit var weight: String

}
