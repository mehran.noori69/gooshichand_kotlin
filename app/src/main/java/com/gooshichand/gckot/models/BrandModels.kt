package com.gooshichand.gckot.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
* Created by MaHkOoM on 1/8/2019.
*/

class BrandModels : Serializable {
    lateinit var name: String
    @SerializedName("img")
    lateinit var image: String
}