package com.gooshichand.gckot.models

class MobileStore {
    lateinit var address: String
    lateinit var city: String
    lateinit var phone: String
    lateinit var price: String
    lateinit var store: String
    lateinit var title: String
    lateinit var website: String
}
