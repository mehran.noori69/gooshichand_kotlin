package com.gooshichand.gckot.tools

import android.graphics.Typeface
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.gooshichand.gckot.MainActivity
import com.gooshichand.gckot.R


/**
* Created by MaHkOoM on 1/8/2019.
*/

class Utility{

    var offerName : String = "پیشنهادی"
    var hasOffer : Boolean = true

    fun makeSnackBar(activity: AppCompatActivity, message: String) {
        val rootView: View = activity.window.decorView.findViewById(android.R.id.content)
        val mysnack = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
        val view: View = mysnack.view
        view.setBackgroundColor(ContextCompat.getColor(activity, R.color.gray))
        val tv: TextView = view.findViewById(android.support.design.R.id.snackbar_text)
        tv.setTextColor(ContextCompat.getColor(activity, R.color.white))
        tv.textSize = activity.resources.getInteger(R.integer.toast_size).toFloat()
        tv.typeface = adjustFont(activity)
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.BOTTOM or Gravity.START
        view.layoutParams = params
        ViewCompat.setLayoutDirection(mysnack.view, ViewCompat.LAYOUT_DIRECTION_RTL)
        mysnack.show()
    }

    fun adjustFont(activity: AppCompatActivity): Typeface {
        return Typeface.createFromAsset(activity.assets, activity.getString(R.string.font))
    }

    fun toPersianNumber(inputs: String): String {
        var input = inputs
        val persian = arrayOf("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹")
        for (j in persian.indices)
            input = input.replace(j.toString(), persian[j])

        return input
    }

    fun changeAppTitle(newTitle: String, activity: MainActivity) : String {
        val tempName : String = activity.getAppTitle().text.toString()
        activity.getAppTitle().text = newTitle
        return tempName
    }
}