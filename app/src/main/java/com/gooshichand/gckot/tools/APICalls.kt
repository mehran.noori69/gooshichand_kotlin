package com.gooshichand.gckot.tools

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


interface APICalls {
    @FormUrlEncoded
    @POST("brandpage/")
    fun getBrandPage(@Field("brand") name: String): Call<JsonObject>

    @GET("homepage/")
    fun getHomePage(): Call<JsonObject>

    @FormUrlEncoded
    @POST("searchitembytitle/")
    fun searchItemByTitle(@Field("start") start: String, @Field("search") search: String): Call<JsonArray>

    @FormUrlEncoded
    @POST("estitembytitle/")
    fun estItemByTitle(@Field("start") start: String, @Field("which") which: String,
                       @Field("brand") brand: String): Call<JsonArray>

    @FormUrlEncoded
    @POST("contactus/")
    fun contactUs(@Field("name") name: String, @Field("email") email: String,
                  @Field("section") section: String, @Field("question") question: String,
                  @Field("device_id") device_id: String): Call<JsonObject>

}