package com.gooshichand.gckot.tools

/**
* Created by MaHkOoM on 1/8/2019.
*/

import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity

class Preferences(activity: AppCompatActivity) {

    private val mSharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
    private val editor: SharedPreferences.Editor

    init {
        editor = mSharedPreferences.edit()
        editor.apply()
    }

    fun getString(name: String, defaultValue: String): String {
        var value = mSharedPreferences.getString(name, defaultValue)
        if (value == null)
            value = defaultValue
        return value
    }

    fun saveString(name: String, value: String) {
        editor.putString(name, value)
        editor.apply()
    }

}